package com.example.restservice;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import com.example.restservice.Billionaires_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@Autowired
	private BillionairesRepo billionairesRepo;

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	@GetMapping("/database")
	public List<Billionaires> database() {
		return billionairesRepo.findAll();
	}

	@GetMapping("/spec/{keres}")
	public List<Billionaires> spec(@PathVariable String keres) {
		Specification<Billionaires> specification = (root, cq, cb) -> cb.like(root.get(Billionaires_.firstName), "%" + keres + "%");
		return billionairesRepo.findAll(specification);
	}
}
